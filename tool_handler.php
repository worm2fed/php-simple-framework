<?php 
# Author: worm2fed
# v3.0
# upd: 06.09.2016

// Класс реализует набор различных инструментов
Class ToolHandler {
	private function __construct() {

	}

	// Функций генерации хеш-кода
	public static function generate_hash_code(int $length = 6) 
	: string {
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHI JKLMNOPRQSTUVWXYZ0123456789";
		$code = "";
		$clen = strlen($chars) - 1;  
   
		while (strlen($code) < $length)
    		$code .= $chars[mt_rand(0, $clen)]; 

		return $code;
	}

	// Функция для проверки вхождения даты в интервал
	public static function check_is_date_in_range(date $date_from_user, 
											date $end_date = null, 
											date $start_date = null) 
	: bool {
		if (is_null($end_date))
			$end_date = date('Y-m-d');

		if (is_null($start_date))
			$start_date = date('Y-m-d', strtotime('-'.NEW_GOOD_LABEL.' days', strtotime($end_date)));

		// Конвертируем в timestamp
		$start_ts = strtotime($start_date);
		$end_ts = strtotime($end_date);
		$user_ts = strtotime($date_from_user);

		// Проверяем находится ли дата между start и end
		return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
	}

	public static function get_client_ip() 
	: string {
	    if (isset($_SERVER['HTTP_CLIENT_IP']))
	        return $_SERVER['HTTP_CLIENT_IP'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
	        return $_SERVER['HTTP_X_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_X_FORWARDED']))
	        return $_SERVER['HTTP_X_FORWARDED'];
	    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
	        return $_SERVER['HTTP_FORWARDED_FOR'];
	    else if(isset($_SERVER['HTTP_FORWARDED']))
	        return $_SERVER['HTTP_FORWARDED'];
	    else if(isset($_SERVER['REMOTE_ADDR']))
	        return $_SERVER['REMOTE_ADDR'];
	    else
	        return 'UNKNOWN';
	}

	// Функция проверяет, существует ли элемент с заданным id
	public static function check_is_record_exist($table, $id) {
		$where = new DBWhere();
		$where->addWhere($table.'_id', '=', $id);

		return DatabaseHandler::countEntry($table, $table.'_id', $where);
	}	


	// Функция проверки вхождения значения в массив
	public static function array_value_exist($value, $array, $return_id = null,  
									$key = null, $second_key = null, $second_value = null) {
		if (is_null($key)) {
			for ($i = 0; $i < count($array); $i++) {
				if ($array[$i] == $value) {
					if (is_null($return_id))
						return true;
					else 
						return $i;
				}
			}
		} else {
			if (is_null($second_key)) {
				for ($i = 0; $i < count($array); $i++) {
					if ($array[$i][$key] == $value) { 
						if (is_null($return_id))
							return true;
						else 
							return $i;
					}
				}
			} else {
				if (is_null($second_value))
					return false;
				else {
					for ($i = 0; $i < count($array); $i++) {
						if ($array[$i][$key] == $value and 
								$array[$i][$second_key] == $second_value) {
							if (is_null($return_id))
								return true;
							else 
								return $i;
						}	
					}
				}
			}
			
		}

		return false;
	}
}
?>