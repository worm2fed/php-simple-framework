<?php
# Author: worm2fed
# v2.0.5
# upd: 02.06.2016

// Класс, реализующий обработчик персонала
class PersonnelHandler {

	private function __construct() {

	}

	// Функция для добавления персонала
	public static function addPersonnel(string $login, 
							string $password, 
							string $name, 
							int $job) 
	: bool {
		$where = new DBWhere();
		$where->addWhere(Config::PERSONNEL_TABLE.'_login', '=', $login);
		
		$personnel_login_num = DatabaseHandler::countEntry(Config::PERSONNEL_TABLE, Config::PERSONNEL_TABLE.'_id', $where);

   		if ($personnel_login_num != 0)
   			return false;
   		else {
   			$data = new DBData();
   			$data->addData(Config::PERSONNEL_JOB_TABLE.'_id', $job);
   			$data->addData(Config::PERSONNEL_TABLE.'_name', $name);
   			$data->addData(Config::PERSONNEL_TABLE.'_login', $login);
   			$data->addData(Config::PERSONNEL_TABLE.'_password', sha1($password));
					
			return DatabaseHandler::addToTable(Config::PERSONNEL_TABLE, $data);
		}
	}

	// Функция для изменения должности персонала
	public static function changePersonnelJob(int $personnel, 
									int $job) 
	: bool {
		$data = new DBData();
		$data->addData(Config::PERSONNEL_JOB_TABLE.'_id', $job);
   		
   		$where = new DBWhere();
   		$where->addWhere(Config::PERSONNEL_TABLE.'_id', '=', $personnel);
		
		return DatabaseHandler::updateInTable(Config::PERSONNEL_TABLE, $data, $where);
	}

	// Функция для смены пароля
	public static function changePersonnelPassword(int $personnel, 
									string $old_password, 
									string $new_password) 
	: bool {
		$where = new DBWhere();
		$where->addWhere(Config::PERSONNEL_TABLE.'_id', '=', $personnel);
		$where->addWhere(Config::PERSONNEL_TABLE.'_password', '=', sha1($old_password));

		$password_match = DatabaseHandler::countEntry(Config::PERSONNEL_TABLE, Config::PERSONNEL_TABLE.'_password', $where);

		if ($password_match != 1)
			return false;
		else {
			$data = new DBData();
			$data->addData(Config::PERSONNEL_TABLE.'_password', sha1($new_password));
	   		
			$where = new DBWhere();
			$where->addWhere(Config::PERSONNEL_TABLE.'_id', '=', $personnel);

			return DatabaseHandler::updateInTable(Config::PERSONNEL_TABLE, $data, $where);
		}
	}

	// Получить данные о пользователе
	public static function getPersonnelInfo(int $personnel) 
	: array {
		$where = new DBWhere();
		$where->addWhere(Config::PERSONNEL_TABLE.'_id', '=', $personnel);

		return DatabaseHandler::selectFromTable(Config::PERSONNEL_TABLE, null, $where, null, false);
	}

	// Функция получения списка работников
	public static function getPersonnelList() 
	: array {
		$where = new DBWhere();
		$where->addWhere(Config::PERSONNEL_JOB_TABLE.'_id', '!=', 1);

		return DatabaseHandler::selectFromTable(Config::PERSONNEL_TABLE, null, $where);
	}

	// Функция для удаления персонала
	public static function deletePersonnel(int $personnel) 
	: bool {
		return DatabaseHandler::deleteFromTable(Config::PERSONNEL_TABLE, $personnel);
	}

	// Получить данные о должности
	public static function getJobInfo(int $job) 
	: array {
		$where = new DBWhere();
		$where->addWhere(Config::PERSONNEL_JOB_TABLE.'_id', '=', $job);

		return DatabaseHandler::selectFromTable(Config::PERSONNEL_JOB_TABLE, null, $where, null, false);
	}

	// Функция получения списка должностей
	public static function getJobList() 
	: array {
		$where = new DBWhere();
		$where->addWhere(Config::PERSONNEL_JOB_TABLE.'_id', '!=', 1);

		return DatabaseHandler::selectFromTable(Config::PERSONNEL_JOB_TABLE, null, $where);
	}
}
?>