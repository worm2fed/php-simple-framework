<?php
# Author: worm2fed
# v3.0.0
# upd: 19.08.2016

// Класс, предостовляющий базовую функциональность доступа к данным
class DatabaseHandler {
	// Переменная для хранения экземпляра класса
  	private static $_mHandler;

  	// private-конструктор, не позволяющий напрямую создавать объекты класса
  	private function __construct() {
  	}

  	// Возвращает инициализированный дескриптор БД
  	private static function getHandler() 
  	: mysqli {
    	// Создаём соединение с БД, только если его ещё нет
	    if (is_null(self::$_mHandler)) {
	     	// Выполняем код, перехватывающий потенциальные исключения
	    	try {
	        	// Создаём новый экземпляр класса mysqli
	        	self::$_mHandler = new mysqli(Config::SQL_HOST, Config::SQL_USER, Config::SQL_PASS, Config::SQL_DB);
	        	// Устанавливаем кодировку соединения
	        	self::$_mHandler->set_charset("utf8");
	      	} catch (mysqli_sql_exception $e) {
	        	// Закрываем дескриптор и генерируем ошибку
	        	self::closeConnection();
	        	trigger_error($e->getMessage(), E_USER_ERROR);
	      	}
	    }

	    // Возвращаем дескриптор БД
	    return self::$_mHandler;
	}

	// Очищаем экземпляр класса mysqli и закрываем соединение
	private static function closeConnection() 
	: void {
		self::$_mHandler = null;
	}

	// Метод для выполнения sql-запроса к БД
	private static function executeQuery(string $sqlQuery) 
	: bool {
		// Пытаемся выполнить sql-запрос или хранимую процедуру
	    try {
	    	// Получаем дескриптор БД
	    	$database_handler = self::getHandler();

	    	// Подготавливаем запрос
	    	$statement_handler = $database_handler->prepare($sqlQuery);

	    	// Выполняем запрос
	    	return $statement_handler->execute();
	    } catch (mysqli_sql_exception $e) {
	    	// Закрываем дескриптор БД и генерируем ошибку
	    	self::closeConnection();
	    	trigger_error($e->getMessage(), E_USER_ERROR);

	    	return false;
	    }
	}

	// Метод для получения данных из БД
	private static function getData(string $sqlQuery) 
	: array {
	    // Инициализируем возвращаемое значение в null
	    $result = null;

	    // Пытаемся выполнить sql-запрос или хранимую процедуру
	    try {
	    	// Получаем дескриптор БД
	    	$database_handler = self::getHandler();

	    	// Подготавливаем запрос
	    	$statement_handler = $database_handler->prepare($sqlQuery);

	    	// Выполняем запрос
	    	$statement_handler->execute();

	    	// Получаем результат
	    	$get_result = $statement_handler->get_result();
	    	$result = $get_result->fetch_all(MYSQLI_ASSOC);
			$result['num_rows'] = $get_result->num_rows;
	    } catch (mysqli_sql_exception $e) {
	    	// Закрываем дескриптор БД и генерируем ошибку
	    	self::closeConnection();
	    	trigger_error($e->getMessage(), E_USER_ERROR);
	    }

	    // Возвращаем результат
	    return $result ?? array();
	}

	// Метод для получения данных из БД
	private static function getRow(string $sqlQuery) 
	: array {
	    // Инициализируем возвращаемое значение в null
	    $result = null;

	    // Пытаемся выполнить sql-запрос или хранимую процедуру
	    try {
	    	// Получаем дескриптор БД
	    	$database_handler = self::getHandler();

	    	// Подготавливаем запрос
	    	$statement_handler = $database_handler->prepare($sqlQuery);

	    	// Выполняем запрос
	    	$statement_handler->execute();

	    	// Получаем результат
	    	$get_result = $statement_handler->get_result();
	    	$result = $get_result->fetch_assoc();
	    } catch (mysqli_sql_exception $e) {
	    	// Закрываем дескриптор БД и генерируем ошибку
	    	self::closeConnection();
	    	trigger_error($e->getMessage(), E_USER_ERROR);
	    }

	    // Возвращаем результат
	    return $result ?? array();
	}

	// Получить id последней операции
	public static function getLastInsertId() 
	: int {
		try {
	    	// Получаем дескриптор БД
	    	$database_handler = self::getHandler();

	    	// Получаем id
	    	return $database_handler->insert_id;
	    } catch (mysqli_sql_exception $e) {
	    	// Закрываем дескриптор БД и генерируем ошибку
	    	self::closeConnection();
	    	trigger_error($e->getMessage(), E_USER_ERROR);

	    	return -1;
	    }
	}

	// Метод для удаления данных из таблицы
	public static function deleteFromTable(string $name, 
										int $id, 
										string $foreign_where = null) 
	: bool {
		// Запрос на удаление эл-та
		$sqlQuery = "DELETE 
					 FROM `" .$name. "` 
					 WHERE " .($foreign_where ?? $name). "_id = '" .$id. "'";

		return self::executeQuery($sqlQuery);
	}

	// Считаем количнство записей в базе
	public static function countEntry(string $table, 
									string $count_field, 
									DBWhere $where) 
	: int {
		$result = null;

		$sqlQuery = "SELECT COUNT(" .$count_field. ") 
					 AS num 
					 FROM `" .$table. "` 
					 WHERE ";

		for ($i = 0; $i < $where->getNum(); $i++) {
			$sqlQuery .= $where->getField()[$i]." ";
			$sqlQuery .= $where->getOperation()[$i]." ";
			$sqlQuery .= ($where->getValue()[$i] == 'null') ? "null" : "'".$where->getValue()[$i]."'";

			if ($i < $where->getNum() - 1)
				$sqlQuery .= " AND ";
		}

   		return self::getRow($sqlQuery)['num'];
	}

	// Добавить запись в базу
	public static function addToTable(string $table, 
									DBData $data) 
	: bool {
		$sqlQuery = "INSERT INTO `" .$table. "` (";
			
		foreach ($data->getColumn() as $column)
			$sqlQuery .= $column. ", ";
		$sqlQuery = substr($sqlQuery, 0, -2);
			
		$sqlQuery .= ") VALUES (";
		
		foreach ($data->getValue() as $value) {
			if ($value == 'null')
				$sqlQuery .= "null, ";
			else
				$sqlQuery .= "'" .$value. "', ";
		}
		$sqlQuery = substr($sqlQuery, 0, -2);
			
		$sqlQuery .= ")";

		return self::executeQuery($sqlQuery);
	}

	// Получить всё из таблицы
	public static function selectFromTable(string $table, 
										DBColumn $column = null, 
										DBWhere $where = null, 
										DBParam $param = null, 
										bool $get_data = true) 
	: array {
		$sqlQuery = "SELECT ";

		// Если не установлена колонка, то извлекаем все
		if (is_null($column))
			$sqlQuery .= "*";
		else {
			foreach ($column->getColumn() as $field)
				$sqlQuery .= $field. ","; 
			$sqlQuery = substr($sqlQuery, 0, -1);
		}

		$sqlQuery .= " FROM `" .$table. "`";

		if (!is_null($where)) {
			$sqlQuery .= " WHERE ";
			for ($i = 0; $i < $where->getNum(); $i++) {
				$sqlQuery .= $where->getField()[$i]." ";
				$sqlQuery .= $where->getOperation()[$i]." ";
				$sqlQuery .= ($where->getValue()[$i] == 'null') ? "null" : 
					(count(explode('()', $where->getValue()[$i])) == 2) ? $where->getValue()[$i] : "'".$where->getValue()[$i]."'";

				if ($i < $where->getNum() - 1)
					$sqlQuery .= " AND ";
			}
	   	}

	   	// Добавляем параметры, если есть
	   	if (!is_null($param)) {
			foreach ($param->getParam() as $param)
				$sqlQuery .= " " . $param;
		}

		if ($get_data)
			return self::getData($sqlQuery);
		else 
			return self::getRow($sqlQuery);
	}

	// Обновить данные в таблице
	public static function updateInTable(string $table, 
									DBData $data, 
									DBWhere $where) 
	: bool {
		$sqlQuery = "UPDATE `" .$table. "` SET ";
		
		for ($i = 0; $i < count($data->getColumn()); $i++)
			$sqlQuery .= $data->getColumn()[$i]. " = '" .$data->getValue()[$i]. "',";
		$sqlQuery = substr($sqlQuery, 0, -1);

		$sqlQuery .= " WHERE ";

		for ($i = 0; $i < $where->getNum(); $i++) {
			$sqlQuery .= $where->getField()[$i]." ";
			$sqlQuery .= $where->getOperation()[$i]." ";
			$sqlQuery .= ($where->getValue()[$i] == 'null') ? "null" : "'".$where->getValue()[$i]."'";

			if ($i < $where->getNum() - 1)
				$sqlQuery .= " AND ";
		}

		return self::executeQuery($sqlQuery);
	}
}

// Стуркутра данных
Class DBData {
	private $column;
	private $value;
	private $size;

	public function __construct() {
		$this->column = array();
		$this->value = array();
		$this->size = 0;
	}

	// Получить массив столбцов
	public function getColumnList() 
	: array {
		return $this->column;
	}

	// Получить столбец
	public function getColumn(int $pos) {
		return ($pos >= $this->size) ? false : $this->column[$pos];
	}

	// Получить массив значений
	public function getValueList() 
	: array {
		return $this->value;
	}

	// Получить значение
	public function getValue(int $pos) {
		return ($pos >= $this->size) ? false : $this->value[$pos];
	}

	// Получить количество записей
	public function getSize() 
	: int {
		return $this->size;
	}

	// Добавить запись
	public function addData(string $column, 
						string $value) {
		$this->column[$this->size] = $column;
		$this->value[$this->size] = $value;
		$this->size++;
	}
}

// Структура столбцов
Class DBColumn {
	private $column;
	private $size;

	public function __construct() {
		$this->column = array();
		$this->size = 0;
	}

	public function getColumnList() 
	: array {
		return $this->column;
	}

	public function getColumn(int $pos) {
		return ($pos >= $this->size) ? false : $this->column[$pos];
	}

	public function getSize() 
	: int {
		return $this->size;
	}

	public function addColumn(string $column) {
		$this->column[$this->size] = $column;
		$this->size++;
	}
}

// Структура условий
Class DBWhere {
	private $field;
	private $operation;
	private $value;
	private $size;

	public function __construct() {
		$this->field = array();
		$this->operation = array();
		$this->value = array();
		$this->size = 0;
	}

	public function getFieldList() 
	: array {
		return $this->field;
	}

	public function getField(int $pos) {
		return ($pos >= $this->size) ? false : $this->field[$pos];
	}

	public function getOperationList() 
	: array {
		return $this->operation;
	}

	public function getOperation(int $pos) {
		return ($pos >= $this->size) ? false : $this->operation[$pos];
	}

	public function getValueList() 
	: array {
		return $this->value;
	}

	public function getValue(int $pos) {
		return ($pos >= $this->size) ? false : $this->value[$pos];
	}

	public function getSize() 
	: int {
		return $this->size;
	}

	public function addWhere(string $field, 
						string $operation, 
						string $value) {
		$this->field[$this->size] = $field;
		$this->operation[$this->size] = $operation;
		$this->value[$this->size] = $value;
		$this->size++;
	}
}

// Структура параметров
Class DBParam {
	private $param;
	private $size;

	public function __construct() {
		$this->param = array();
		$this->size = 0;
	}

	public function getParamList() 
	: array {
		return $this->param;
	}

	public function getParam(int $pos) {
		return ($pos >= $this->size) ? false : $this->param[$pos];
	}

	public function getSize() 
	: int {
		return $this->size;
	}

	public function addParam(string $param) {
		$this->param[$this->size] = $param;
		$this->size++;
	}
}
?>
