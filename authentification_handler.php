<?php
# Author: worm2fed
# v2.0.5
# upd: 21.05.2016

// Класс, реализующий аутентификацию пользователей
class AuthentificationHandler {
	private $_mTable = null;

	public function __construct(string $table) {
		// инициализируем таблицу (кого будем авторизировать персонал, пользователей и т.д.)
		$this->_mTable = $table;
	}

	// Авторизовать пользователя
	public function loginUser(string $login, 
						string $password, 
						int $child_store = null) 
	: bool {
		$where = new DBWhere();
		$where->addWhere($this->_mTable.'_login', '=', $login);
		$where->addWhere($this->_mTable.'_password', '=', sha1($password));

		$data_match = DatabaseHandler::countEntry($this->_mTable, $this->_mTable.'_id', $where);

		if ($data_match != 1)
			return false;
		else {
			$column = new DBColumn();
			$column->addColumn($this->_mTable.'_id');
			$column->addColumn(Config::PERSONNEL_JOB_TABLE.'_id');

			$where = new DBWhere();
			$where->addWhere($this->_mTable.'_login', '=', $login);
			$where->addWhere($this->_mTable.'_password', '=', sha1($password));

			$user_data = DatabaseHandler::selectFromTable($this->_mTable, $column, $where, null, false);
			$user_id = $user_data[$this->_mTable.'_id'];
			$job_id = $user_data[Config::PERSONNEL_JOB_TABLE.'_id'];

			if (is_null($child_store) and $job_id == 3)
				return false;

			$hash = md5(ToolHandler::generate_hash_code(10));

			$data = new DBData();
			$data->addData($this->_mTable.'_hash', $hash);
	   		
	   		$where = new DBWhere();
	   		$where->addWhere($this->_mTable.'_id', '=', $user_id);

			DatabaseHandler::updateInTable($this->_mTable, $data, $where);
      		
      		// Если активный пользователь с указанными данными найден, стартуем сессию.
      		$time = time() + 3600 * Config::SESSION_TIME;
      		setCookie(Config::VAR_NAME . '_' .$this->_mTable. '_hash', $hash, $time);

      		$_SESSION[$this->_mTable.'_id'] = $user_id;
      		$_SESSION[$this->_mTable.'_child_store'] = $child_store;
      		$_SESSION[$this->_mTable.'_solt'] = md5(crypt($user_id, $hash)); 

      		// Добавляем отчёт
      		ReportHandler::addAuthReport($user_id, $child_store); 
      		
      		return true;
   		}
	}

	// Проверить неподменены ли данные
	public function checkIsUserLogined() 
	: bool {
		if (isset($_COOKIE[Config::VAR_NAME . '_' .$this->_mTable. '_hash']) and 
				isset($_SESSION[$this->_mTable.'_id'])) {
			if ($_SESSION[$this->_mTable.'_solt'] != md5(crypt($_SESSION[$this->_mTable.'_id'], $_COOKIE[Config::VAR_NAME . '_' .$this->_mTable. '_hash'])))
				return false;
			else {
				$column = new DBColumn();
				$column->addColumn($this->_mTable.'_id');
				$column->addColumn($this->_mTable.'_hash');

				$where = new DBWhere();
				$where->addWhere($this->_mTable.'_id', '=', $_SESSION[$this->_mTable.'_id']);

				$user_data = DatabaseHandler::selectFromTable($this->_mTable, $column, $where, null, false);

				if (($user_data[$this->_mTable.'_hash'] != $_COOKIE[Config::VAR_NAME . '_' .$this->_mTable. '_hash']) or 
						($user_data[$this->_mTable.'_id'] != $_SESSION[$this->_mTable.'_id'])) {
					self::logout();
					return false;
				} else 
					return true;
			}
    	} else
			return false;
	}

	// Завершить сеанс
	public function logout() {
		setCookie(Config::VAR_NAME . '_' .$this->_mTable. '_hash', '', time() -3600 * Config::SESSION_TIME, '');
		session_unset();
		session_destroy();
	}
}
