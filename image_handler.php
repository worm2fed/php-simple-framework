<?php
# Author: worm2fed
# v2.0
# upd: 18.05.2016

// Класс реализует обработчик изображений
class ImageHandler {
   private $image;
   private $image_type;

   // Загрузка изображения
   public function load(string $filename) 
   : void {
      $image_info = getimagesize($filename);
      $this->image_type = $image_info[2];

      if ($this->image_type == IMAGETYPE_JPEG)
         $this->image = imagecreatefromjpeg($filename);
      elseif ($this->image_type == IMAGETYPE_GIF)
         $this->image = imagecreatefromgif($filename);
      elseif ($this->image_type == IMAGETYPE_PNG)
         $this->image = imagecreatefrompng($filename);
   }

   // Сохранение на сервер
   function save(string $filename, 
               $image_type = IMAGETYPE_JPEG, 
               int $compression = 75, 
               $permissions = null) 
   : void {
      if ($image_type == IMAGETYPE_JPEG)
         imagejpeg($this->image, $filename, $compression);
      elseif ($image_type == IMAGETYPE_GIF)
         imagegif($this->image, $filename);
      elseif ($image_type == IMAGETYPE_PNG)
         imagepng($this->image, $filename);
      
      if ($permissions != null)
         chmod($filename, $permissions);
   }

   // Выыводит изображение в браузер без сохранения
   function output($image_type = IMAGETYPE_JPEG) 
   : void {
      if ($image_type == IMAGETYPE_JPEG)
         imagejpeg($this->image);
      elseif ($image_type == IMAGETYPE_GIF)
         imagegif($this->image);
      elseif ($image_type == IMAGETYPE_PNG)
         imagepng($this->image);
   }

   // Получить ширину
   function getWidth() 
   : int {
      return imagesx($this->image);
   }

   // Получить высоту
   function getHeight() 
   : int {
      return imagesy($this->image);
   }

   // Изменение высоты
   function resizeToHeight(int $height) 
   : void {
      $ratio = $height / $this->getHeight();
      $width = $this->getWidth() * $ratio;
      $this->resize($width, $height);
   }

   // Изменение ширины
   function resizeToWidth(int $width) 
   : void {
      $ratio = $width / $this->getWidth();
      $height = $this->getheight() * $ratio;
      $this->resize($width, $height);
   }

   // Изменение размеров в процентах
   function scale(int $scale) 
   : void {
      $width = $this->getWidth() * $scale/100;
      $height = $this->getheight() * $scale/100;
      $this->resize($width, $height);
   }

   // Изменение ширины и высоты
   function resize(int $width, 
               int $height) 
   : void {
      $new_image = imagecreatetruecolor($width, $height);
      imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
      $this->image = $new_image;
   }

   // Функция отражение по горизонтали (true) или векртикали (false)
   function reflectImage(bool $vertical = true) 
   : void {
      $width = $this->getWidth();
      $height = $this->getheight();
      $new_image = imagecreatetruecolor($width, $height);
      
      if ($vertical) {
         for ($i = 0; $i < $width; $i++) {
            for ($j = 0; $j < $height; $j++) {
               $color = imagecolorat($this->image, $i, $j);
               imagesetpixel($new_image, $width - $i - 1, $j, $color);
            }
         }
      } else {
         for ($i = 0; $i < $width; $i++) {
            for ($j = 0; $j < $height; $j++) {
               $color = imagecolorat($this->image, $i, $j);
               imagesetpixel($new_image, $i, $height - $j - 1, $color);
            }
         }
      }

      $this->image = $new_image;
   }
}
?>